import React, { Component } from "react";
import axios from "axios";
import YTDChartComponent from "./YTDChartComponent";
import SingleDropDownComponent from "./SingleDropDownComponent";
import DrillButtonsComponent from "./DrillButtonsComponent";
import {
  Button,
  Navbar,
  Nav,
  Form,
  FormControl,
  Card,
  Alert,
} from "react-bootstrap";
import "reactjs-popup/dist/index.css";
import Popup from "reactjs-popup";
const Months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];
class YTDChartMainComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      year: this.props.year,
      Category: this.props.Category,
      CategoryOptions: [],
      PositionIDStatus: this.props.PositionIDStatus,
      YearOptions: [],
      YearSelected: [{ Year: this.props.year }],
    };
  }

  async componentDidMount() {
    const { data } = await axios.get("http://localhost:8080/api/v1/YTD", {
      params: {
        reqYear: this.state.year,
        reqCategory: this.state.Category,
        reqPositionIDStatus: this.state.PositionIDStatus,
      },
    });
    await this.setData(data);
    const { data: minYear } = await axios.get(
      "http://localhost:8080/api/v1/Years"
    );
    const YearOptions = [];
    for (let i = minYear; i <= new Date().getFullYear(); i++) {
      YearOptions.push({ Year: i });
    }
    await this.setState({
      YearOptions: YearOptions,
    });
  }

  setData = (data) => {
    let CategoryOptions = new Set();
    const modifiedData = [];
    for (let key in data) {
      for (let innerkey in data[key]) {
        CategoryOptions.add(innerkey);
      }
    }
    CategoryOptions = [...CategoryOptions, "Grand Total"];
    let it = 0;
    for (let i of CategoryOptions) {
      it++;
    }
    for (let m of Months) {
      let temp = { month: m };
      let total = 0;
      for (let key in data[m]) {
        temp[key] = data[m][key];
        total += temp[key];
      }
      data[m]["Grand Total"] = total;
      if (total) temp["Grand Total"] = total;
      modifiedData.push(temp);
    }
    let temp = { month: "Grand Total" };
    for (let i of CategoryOptions) {
      let total = 0;
      for (let m of Months) {
        total += data[m][i] === undefined ? 0 : data[m][i];
      }
      temp[i] = total;
    }
    modifiedData.push(temp);
    // console.log(data);
    // console.log(CategoryOptions);
    // console.log(modifiedData);
    this.setState({
      data: modifiedData,
      CategoryOptions: CategoryOptions,
    });
  };

  drillFunction = async (pivot) => {
    // console.log("drill");
    const { data: Category } = await axios.get(
      "http://localhost:8080/api/v1/drill",
      {
        params: { label: this.state.Category, pivot: pivot },
      }
    );
    // console.log(Category);
    if (Category == this.state.Category) return;
    await this.setState({ Category: Category });
    const { data } = await axios.get("http://localhost:8080/api/v1/YTD", {
      params: {
        reqYear: this.state.year,
        reqCategory: this.state.Category,
        reqPositionIDStatus: this.state.PositionIDStatus,
      },
    });
    // console.log(data);
    await this.setData(data);
  };

  updateGraph = async (selectedItems) => {
    const year = selectedItems[0].Year;
    await this.setState({ YearSelected: selectedItems, year: year });
    const { data } = await axios.get("http://localhost:8080/api/v1/YTD", {
      params: {
        reqYear: this.state.year,
        reqCategory: this.state.Category,
        reqPositionIDStatus: this.state.PositionIDStatus,
      },
    });
    await this.setData(data);
  };

  saveGraphToDashboard = async () => {
    // await this.stackref.current.saveCharts3();
    console.log("really123");
    console.log(this.state.Category1, "okkkkkkk");
    let newFilterList = [];
    let temp = new Object();
    temp["category"] = "Position Id Status";
    temp["name"] = this.state.PositionIDStatus;
    newFilterList.push(temp);
    temp = new Object();
    temp["category"] = "Year";
    temp["name"] = this.state.year;
    newFilterList.push(temp);
    const { data: message1 } = await axios.post(
      "http://localhost:8080/api/users/updateYTDChart",
      newFilterList,
      {
        params: {
          id: this.props.YTDid,
          Category1: this.state.Category,
        },
      }
    );
  };

  render() {
    const styletext1 = {
      fontSize: "16px",
      fontWeight: "bold",
      // textDecoration: "underline",

      fontFamily:
        "'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif",
    };
    const styletext2 = {
      fontSize: "14px",
      fontWeight: "bold",

      fontFamily:
        "'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif",
    };
    const styletext3 = {
      fontSize: "14px",
      fontWeight: "bold",
      // fontStyle: "italic",
      textAlign: "center",

      fontFamily:
        "'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif",
    };

    const styleChartHead = {
      // marginTop: "15px",
      borderBottom: "2px solid #030921",
      // padding: "10px",
      // textAlign: "center",
      // fontSize: '16px',
      // fontWeight: 'bold',
      // color: 'black',
    };
    return (
      <div>
        {this.props.posi == "left" && (
          <div className="row" style={styleChartHead}>
            <div className="col-lg-7">
              <p style={styletext1}> {this.props.ytdname} </p>
              <p style={styletext2}>
                YTD {this.state.PositionIDStatus} - {this.state.year} -{" "}
                {this.state.Category}{" "}
              </p>
            </div>
            <div className="col-lg-5 hide">
              <div className="row">
                {/* <div className="col-lg-7">
                    <SingleDropDownComponent
                      options={this.state.YearOptions}
                      selectedValues={this.state.YearSelected}
                      name="Year"
                      updateGraph={this.updateGraph}
                    />
                    <p style={styletext3}> Select Year </p>
                  </div> */}
                <div className="col-lg-2"></div>
                <div className="col-lg-5" style={{ marginTop: "1px" }}>
                  <DrillButtonsComponent drillFunction={this.drillFunction} />
                </div>
                {/* <div className="col-lg-1"></div> */}
                <div className="col-lg-5">
                  {/* <Popup
                      contentStyle={{ width: "18rem" }}
                      trigger={
                        open => (
                          // <button className="button">Trigger </button>
                          <Button variant="primary">Primary - {open ? 'Opened' : 'Closed'}</Button>
                        )
                      
                      }
                      position="right center"
                      modal
                    >
                      {(close) => <div>
                        <SingleDropDownComponent
                      options={this.state.YearOptions}
                      selectedValues={this.state.YearSelected}
                      name="Year"
                      updateGraph={this.updateGraph}
                    />
                    <p style={styletext3}> Select Year </p>
                        </div>}
                    </Popup> */}

                  <Popup
                    contentStyle={{ width: "18rem" }}
                    trigger={(open) => (
                      <Button variant="primary" style={{ fontSize: "19px" }}>
                        Filter{" "}
                      </Button>

                      // <button className="button">
                      //   Trigger
                      // </button>
                    )}
                    position="right center"
                    closeOnDocumentClick
                  >
                    <div>
                      <SingleDropDownComponent
                        options={this.state.YearOptions}
                        selectedValues={this.state.YearSelected}
                        name="Year"
                        updateGraph={this.updateGraph}
                      />
                      <p style={styletext3}> Select Year </p>
                    </div>
                    {/* <span> Popup content </span> */}
                  </Popup>
                </div>
              </div>
            </div>
          </div>
        )}

        {this.props.posi == "right" && (
          <div className="row" style={styleChartHead}>
            <div className="col-lg-5 hide">
              <div className="row">
                {/* <div className="col-lg-1"></div> */}
                <div className="col-lg-4">
                  <Popup
                    contentStyle={{ width: "18rem" }}
                    trigger={(open) => (
                      <Button variant="primary" style={{ fontSize: "19px" }}>
                        Filter{" "}
                      </Button>

                      // <button className="button">
                      //   Trigger
                      // </button>
                    )}
                    position="left center"
                    closeOnDocumentClick
                  >
                    <div>
                      <SingleDropDownComponent
                        options={this.state.YearOptions}
                        selectedValues={this.state.YearSelected}
                        name="Year"
                        updateGraph={this.updateGraph}
                      />
                      <p style={styletext3}> Select Year </p>
                    </div>
                    {/* <span> Popup content </span> */}
                  </Popup>
                </div>
                {/* <div className="col-lg-2"></div> */}
                <div
                  className="col-lg-5"
                  style={{ marginTop: "1px", marginLeft: "9px" }}
                >
                  <DrillButtonsComponent drillFunction={this.drillFunction} />
                </div>
              </div>
            </div>

            <div className="col-lg-7" style = {{textAlign: "right"}}>
              <p style={styletext1}> {this.props.ytdname} </p>
              <p style={styletext2}>
                YTD {this.state.PositionIDStatus} - {this.state.year} -{" "}
                {this.state.Category}{" "}
              </p>
            </div>
          </div>
        )}

        {/* <div className="row"> */}
        {/* <div className="col-lg-7">
            <p style={styletext1}> {this.props.ytdname}   </p>
            <p style={styletext2}>YTD {this.state.PositionIDStatus} - {this.state.year} - {this.state.Category} </p>
          </div> */}
        {/* <div className="col-lg-5 hide">
            <div className="row">
              <div className="col-lg-9">
                <SingleDropDownComponent
                  options={this.state.YearOptions}
                  selectedValues={this.state.YearSelected}
                  name="Year"
                  updateGraph={this.updateGraph}
                />
                <p style={styletext3}> Select Year </p>
              </div>
              <div className="col-lg-3" style={{ marginTop: "1px" }}>
                <DrillButtonsComponent drillFunction={this.drillFunction} />
              </div>
            </div>
          </div> */}
        {/* </div> */}

        <br />
        {/* <div> */}
        <YTDChartComponent
          data={this.state.data}
          CategoryOptions={this.state.CategoryOptions}
          colorbar={this.props.colorbar}
        />
        {/* </div> */}
      </div>
    );
  }
}

export default YTDChartMainComponent;
