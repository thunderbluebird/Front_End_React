import React, { Component } from "react";
import { Alert, Card } from "react-bootstrap";
import axios from "axios";
import StackchartMainComponent from "./StackchartMainComponent";
import YTDChartMainComponent from "./YTDChartMainComponent";
import BannerComponent from "./BannerComponent";
import ReactToPrint from "react-to-print";

const colors = ["#085da6", "#32686b", "#5b577d"];
const styleStackchartmain = {
  marginTop: "15px",
  border: "3px solid #030921",
  padding: "20px",
  // textAlign: "center",
  // fontSize: '16px',
  // fontWeight: 'bold',
  // color: 'black',
};
const YTDchartmain = {
  marginTop: "15px",
  border: "3px solid #030921",
  padding: "20px",
  // textAlign: "center",
};
function isNumeric(num) {
  return !isNaN(num);
}

class DashboardComponent extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.AllCharts);
    this.stackref = React.createRef([]);
    this.stackref.current = [];
  }

  componentDidMount() {
    console.log(this.props.dashboard);
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.dashboard != this.props.dashboard) {
      this.stackref.current = [];
    }
  }

  saveCharts2 = async () => {
    for (let el of this.stackref.current) {
      await el.saveGraphToDashboard();
    }
    // await this.stackref.current.saveCharts3();
    // console.log("again");
  };

  addToStackRefs = async (el) => {
    console.log(this.stackref.current);
    if (el && !this.stackref.current.includes(el)) {
      this.stackref.current.push(el);
    }
  };

  chartAdd = (chart, posi) => {
    if (chart.type == "Stacked Bar Chart" || chart.type == "Bar Chart") {
      let isStack = false;
      if (chart.type == "Stacked Bar Chart") {
        isStack = true;
      }
      return (
        <div>
          <div style={styleStackchartmain}>
            <StackchartMainComponent
              ref={this.addToStackRefs}
              stackid={chart.id}
              Category1={chart.category_1}
              Category2={chart.category_2}
              viewname={chart.name}
              filtervals={chart.filtervals}
              isStack={isStack}
              posi={posi}
            />
          </div>
          {/* {(count%2!=0 && count!=len) && <div className="page-break"> </div>} */}
        </div>
      );
    } else if (chart.type == "YTD Chart") {
      let curryear;
      let currposstatus;
      if (isNumeric(chart.filtervals[0])) {
        curryear = chart.filtervals[0];
        currposstatus = chart.filtervals[1];
      } else {
        curryear = chart.filtervals[1];
        currposstatus = chart.filtervals[0];
      }
      const random = Math.floor(Math.random() * colors.length);
      return (
        <div>
          <div style={YTDchartmain}>
            <YTDChartMainComponent
              ref={this.addToStackRefs}
              YTDid={chart.id}
              year={curryear}
              Category={chart.category_1}
              PositionIDStatus={currposstatus}
              colorbar={colors[random]}
              ytdname={chart.name}
              posi={posi}
            />
          </div>
          {/* {(count%2!=0 && count!=len) && <div className="page-break"> </div>} */}
        </div>
      );
    }
  };

  render() {
    const styleDashboardHeading = {
      textAlign: "center",
      marginTop: "-14px",
      fontSize: "26px",
      fontWeight: "400",
    };

    let charts_show = [];
    let len = this.props.AllCharts.length;
    for (let count = 0; count < this.props.AllCharts.length; count += 2) {
      let chart1 = this.props.AllCharts[count];
      let chart2 = this.props.AllCharts[count + 1];
      charts_show.push(
        <div>
          <div className="row">
            <div className="col-lg-6">{this.chartAdd(chart1, "left")}</div>
            {count != len - 1 && (
              <div className="col-lg-6">{this.chartAdd(chart2, "right")}</div>
            )}
          </div>
          <br />
          <br />
          {((len%2==0 && count!=len-2) || (len%2!=0 && count!=len-1) ) && <div className="page-break"> </div>}
          </div>
      );
    }
    const styleCard = {
      marginTop: "90px",
      marginLeft: "auto",
      marginRight: "auto",
      // margin: "auto",
      // textAlign: "center",
      //   width: 50%;
      height: "200px",
      width: "415px",
      fontSize: "28px",
    };
    return (
      <div>
                  <br />

         {this.props.AllCharts.length != 0 && (
                    <div   style={{ display: "flex", float:"right" }} >
                      <ReactToPrint
                        trigger={() => (
                          <button
                            className="btn btn-primary hide"
                            style={{
                              marginLeft: "9px",
                              // marginRight: "auto",
                              // marginBottom: "55px",
                              marginTop: "5px",
                            }}
                          >
                            Publish
                          </button>
                        )}
                        content={() => this.componentRef}
                      />
                    </div>
                  )}
        <div ref={(el) => (this.componentRef = el)}>
          {/* <BannerComponent /> */}


          <Card style={{ height: "51px" }}>
            <Card.Body style={styleDashboardHeading}>
            Dashboard- {this.props.dashboard.name}
            </Card.Body>
          </Card>
          {this.props.AllCharts.length == 0 && (
            <div>
              <Card body style={styleCard}>
                There are no charts in the dashboard. Go to File- Add Chart to
                add one.
              </Card>
            </div>
          )}
          <br />
          {charts_show}
        </div>
      </div>
    );
  }
}

export default DashboardComponent;
