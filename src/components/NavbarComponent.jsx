import React, { Component } from "react";
import {
  Button,
  Navbar,
  Nav,
  Form,
  FormControl,
  NavDropdown,
  Card,
  Alert,
  Col,
} from "react-bootstrap";
import "reactjs-popup/dist/index.css";
import Popup from "reactjs-popup";
import axios from "axios";
import ReactToPrint from "react-to-print";

class NavbarComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newDashboardName: "",
      checkDefault: false,
      currDashboardName: this.props.currentDashboard.name,
      currCheckDefault: this.props.currentDashboard.isDefault,
      loadedDashboardId: -1,
      listOfDashboards: [],
      newChartName: "",
      newChartType: "Bar Chart",
      newCat1: "UBR Level 2",
      newCat2: "Total",
      newAgg: "Count",
    };
  }

  async componentDidMount() {
    await this.getlistofDashboards();
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.currentDashboard != this.props.currentDashboard) {
      this.setState({
        currDashboardName: this.props.currentDashboard.name,
        currCheckDefault: this.props.currentDashboard.isDefault,
      });
    }
  }

  getlistofDashboards = async () => {
    console.log("called");
    const { data: listOfDashboards } = await axios.get(
      "http://localhost:8080/api/users/getListOfDashboard"
    );
    let loadedDashboardId = -1;
    if (listOfDashboards.length) {
      loadedDashboardId = listOfDashboards[0].id;
    }
    console.log(listOfDashboards, loadedDashboardId);
    this.setState({ listOfDashboards, loadedDashboardId });
  };

  newDashboardNameHandler = (event) => {
    this.setState({ newDashboardName: event.target.value });
  };
  currDashboardNameHandler = (event) => {
    this.setState({ currDashboardName: event.target.value });
  };
  newChartNameHandler = (event) => {
    this.setState({ newChartName: event.target.value });
  };
  checkDefaultHandler = (event) => {
    this.setState({ checkDefault: !this.state.checkDefault });
  };

  currCheckDefaultHandler = (event) => {
    this.setState({ currCheckDefault: !this.state.currCheckDefault });
  };
  handleNewDashbaord = async (e) => {
    e.preventDefault();
    this.props.addNewDashboard({
      isDefault: this.state.checkDefault,
      name: this.state.newDashboardName,
    });
  };
  dashboardIdHandler = async (e) => {
    e.preventDefault();
    console.log(e.target.value);
    this.setState({ loadedDashboardId: e.target.value });
  };

  newChartTypeHandler = async (e) => {
    e.preventDefault();
    await this.setState({ newChartType: e.target.value });
    console.log(this.state.newChartType);
    if (this.state.newChartType === "Stacked Bar Chart") {
      await this.setState({ newCat2: "Position Id Status" });
    } else if (this.state.newChartType === "YTD Chart") {
      await this.setState({ newCat2: "Offered" });
    }
    console.log(this.state.newCat2);
  };
  newCat1Handler = async (e) => {
    e.preventDefault();
    this.setState({ newCat1: e.target.value });
  };

  newCat2Handler = async (e) => {
    e.preventDefault();
    this.setState({ newCat2: e.target.value });
  };

  newAggHandler = async (e) => {
    e.preventDefault();
    this.setState({ newAgg: e.target.value });
  };

  render() {
    console.log(this.state.currDashboardName, "test1");
    console.log(this.state.currCheckDefault, "test2");

    let navbarshow;
    console.log(this.state.newChartType === "Stacked Bar Chart");
    if (this.props.isLoggedIn) {
      navbarshow = (
        <div>
          <Navbar
            collapseOnSelect
            expand="lg"
            bg="dark"
            variant="dark"
            className="fixed-top"
          >
            <Navbar.Brand href="#">OPT Dashboard</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <NavDropdown title="File" id="collasible-nav-dropdown">
                  <NavDropdown.Item>
                    <Popup
                      contentStyle={{ width: "28rem" }}
                      trigger={<p>Add Dashboard</p>}
                      position="right center"
                      onClose={() =>
                        this.setState({
                          newDashboardName: "",
                          checkDefault: false,
                        })
                      }
                      modal
                    >
                      {(close) => (
                        <Card style={{ maxWidth: "28rem" }}>
                          <Card.Header as="h5" style={{ textAlign: "center" }}>
                            Add new Dashboard
                          </Card.Header>
                          <Card.Body>
                            <Form>
                              <Form.Group>
                                <Form.Label>Name:</Form.Label>
                                <Form.Control
                                  type="text"
                                  placeholder="Enter name"
                                  value={this.state.newDashboardName}
                                  onChange={this.newDashboardNameHandler}
                                />
                              </Form.Group>

                              <Form.Group controlId="formBasicCheckbox">
                                <Form.Check
                                  type="checkbox"
                                  label="Default"
                                  checked={this.state.checkDefault}
                                  onChange={this.checkDefaultHandler}
                                />
                              </Form.Group>

                              <div style={{ display: "flex" }}>
                                <Button
                                  variant="primary"
                                  type="submit"
                                  //   onClick={this.handleNewDashbaord}
                                  onClick={async (e) => {
                                    e.preventDefault();
                                    await this.props.addNewDashboard({
                                      isDefault: this.state.checkDefault,
                                      name: this.state.newDashboardName,
                                    });
                                    await this.getlistofDashboards();
                                    close();
                                    //   await this.handleLogin();
                                    //   console.log(this.state.errorFlag);
                                    //   if (this.state.errorFlag === 0) {

                                    //   }
                                  }}
                                  style={{
                                    marginLeft: "auto",
                                    marginRight: "auto",
                                    // marginBottom: "0px",
                                    // marginTop: "0px",
                                  }}
                                >
                                  Add
                                </Button>
                              </div>
                            </Form>
                          </Card.Body>
                        </Card>
                      )}
                    </Popup>
                  </NavDropdown.Item>
                  <NavDropdown.Item
                    className={
                      Object.entries(this.props.currentDashboard).length == 0 &&
                      "disabled"
                    }
                  >
                    <Popup
                      contentStyle={{ width: "37rem" }}
                      trigger={<p>Add Chart</p>}
                      position="right center"
                      onClose={() =>
                        this.setState({
                          newChartName: "",
                          newChartType: "Bar Chart",
                          newCat1: "UBR Level 2",
                          newCat2: "Total",
                          newAgg: "Count",
                        })
                      }
                      modal
                    >
                      {(close) => (
                        <Card style={{ maxWidth: "37rem" }}>
                          <Card.Header as="h5" style={{ textAlign: "center" }}>
                            Add new Chart
                          </Card.Header>
                          <Card.Body>
                            <Form>
                              <Form.Row>
                                <Form.Label column="sm" lg={2}>
                                  Name:
                                </Form.Label>
                                <Col>
                                  <Form.Control
                                    size="sm"
                                    type="text"
                                    placeholder="Enter name"
                                    value={this.state.newChartName}
                                    onChange={this.newChartNameHandler}
                                  />
                                </Col>
                              </Form.Row>
                              <br />

                              <Form.Row controlId="exampleForm.ControlSelect1">
                                <Form.Label column="sm" lg={2}>
                                  Type:
                                </Form.Label>
                                <Col>
                                  <Form.Control
                                    size="sm"
                                    as="select"
                                    value={this.state.newChartType}
                                    onChange={this.newChartTypeHandler}
                                  >
                                    <option value="Bar Chart">Bar Chart</option>
                                    <option value="Stacked Bar Chart">
                                      Stacked Bar Chart
                                    </option>
                                    <option value="YTD Chart">YTD Chart</option>
                                  </Form.Control>
                                </Col>
                              </Form.Row>

                              <br />

                              <Form.Row controlId="exampleForm.ControlSelect1">
                                <Form.Label column="sm" lg={2}>
                                  Category 1:
                                </Form.Label>
                                <Col>
                                  <Form.Control
                                    size="sm"
                                    as="select"
                                    value={this.state.newCat1}
                                    onChange={this.newCat1Handler}
                                  >
                                    <option value="UBR Level 2">
                                      UBR Level 2
                                    </option>
                                    <option value="UBR Level 1">
                                      UBR Level 1
                                    </option>
                                    <option value="Sub BU">Sub BU</option>
                                    {(this.state.newChartType ===
                                      "Stacked Bar Chart" ||
                                      this.state.newChartType ===
                                        "Bar Chart") && (
                                      <option value="Country Desc">
                                        Country Desc
                                      </option>
                                    )}
                                    {(this.state.newChartType ===
                                      "Stacked Bar Chart" ||
                                      this.state.newChartType ===
                                        "Bar Chart") && (
                                      <option value="Location">Location</option>
                                    )}
                                  </Form.Control>
                                </Col>
                              </Form.Row>

                              <br />
                              <div
                                className={
                                  this.state.newChartType === "Bar Chart" &&
                                  "invisible"
                                }
                              >
                                <Form.Row controlId="exampleForm.ControlSelect1">
                                  <Form.Label column="sm" lg={2}>
                                    Category 2:
                                  </Form.Label>
                                  <Col>
                                    <Form.Control
                                      size="sm"
                                      as="select"
                                      value={this.state.newCat2}
                                      onChange={this.newCat2Handler}
                                    >
                                      {this.state.newChartType ===
                                        "Stacked Bar Chart" && (
                                        <option value="Position Id Status">
                                          Position Id Status
                                        </option>
                                      )}
                                      {this.state.newChartType ===
                                        "Stacked Bar Chart" && (
                                        <option value="Gender Desc">
                                          Gender Desc
                                        </option>
                                      )}

                                      {this.state.newChartType ===
                                        "YTD Chart" && (
                                        <option value="Offered">Offered</option>
                                      )}
                                      {this.state.newChartType ===
                                        "YTD Chart" && (
                                        <option value="Open - Approved">
                                          Open - Approved
                                        </option>
                                      )}
                                      {this.state.newChartType ===
                                        "YTD Chart" && (
                                        <option value="On Hold">On Hold</option>
                                      )}
                                    </Form.Control>
                                  </Col>
                                </Form.Row>

                                <br />
                              </div>

                              <Form.Row controlId="exampleForm.ControlSelect1">
                                <Form.Label column="sm" lg={2}>
                                  Aggregation:
                                </Form.Label>
                                <Col>
                                  <Form.Control
                                    size="sm"
                                    as="select"
                                    value={this.state.newAgg}
                                    onChange={this.newAggHandler}
                                  >
                                    <option value="Count">Count</option>
                                  </Form.Control>
                                </Col>
                              </Form.Row>
                              <br />

                              <div style={{ display: "flex" }}>
                                <Button
                                  variant="primary"
                                  type="submit"
                                  //   onClick={this.handleNewDashbaord}
                                  onClick={async (e) => {
                                    e.preventDefault();
                                    await this.props.addNewChart({
                                      name: this.state.newChartName,
                                      type: this.state.newChartType,
                                      category_1: this.state.newCat1,
                                      category_2: this.state.newCat2,
                                      aggregation: this.state.newAgg,
                                    });
                                    // await this.getlistofDashboards();
                                    close();
                                    //   await this.handleLogin();
                                    //   console.log(this.state.errorFlag);
                                    //   if (this.state.errorFlag === 0) {

                                    //   }
                                  }}
                                  style={{
                                    marginLeft: "auto",
                                    marginRight: "auto",
                                    fontSize: "16px",
                                    // marginBottom: "0px",
                                    // marginTop: "0px",
                                  }}
                                >
                                  Add
                                </Button>
                              </div>
                            </Form>
                          </Card.Body>
                        </Card>
                      )}
                    </Popup>
                  </NavDropdown.Item>

                  <NavDropdown.Item
                    className={
                      Object.entries(this.props.currentDashboard).length == 0 &&
                      "disabled"
                    }
                    onClick={this.props.saveCharts}
                  >
                    <p>Save</p>
                  </NavDropdown.Item>
                  {/* <NavDropdown.Item
                    className={
                      Object.entries(this.props.currentDashboard).length == 0 &&
                      "disabled"
                    }
                  >
                    <ReactToPrint
                      trigger={() => <p>Publish</p>}
                      content={() => this.props.dashmainref}
                    />
                  </NavDropdown.Item> */}
                </NavDropdown>

                <NavDropdown title="Dashboard" id="collasible-nav-dropdown">
                  <NavDropdown.Item
                    className={
                      this.state.listOfDashboards.length == 0 && "disabled"
                    }
                  >
                    <Popup
                      contentStyle={{ width: "28rem" }}
                      trigger={<p>Load Dashboard</p>}
                      position="right center"
                      onClose={
                        () => this.getlistofDashboards()
                        // this.setState({
                        //     loadedDashboardId: -1,
                        // })
                      }
                      modal
                    >
                      {(close) => (
                        <Card style={{ maxWidth: "28rem" }}>
                          <Card.Header as="h5" style={{ textAlign: "center" }}>
                            Load Dashboard
                          </Card.Header>
                          <Card.Body>
                            <Form>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>Example select</Form.Label>
                                <Form.Control
                                  as="select"
                                  value={this.state.loadedDashboardId}
                                  onChange={this.dashboardIdHandler}
                                >
                                  {this.state.listOfDashboards.map(
                                    (dashboard) => (
                                      <option value={dashboard.id}>
                                        {dashboard.name}
                                      </option>
                                    )
                                  )}
                                </Form.Control>
                              </Form.Group>

                              <div style={{ display: "flex" }}>
                                <Button
                                  variant="primary"
                                  type="submit"
                                  // onClick={this.handleNewDashbaord}
                                  onClick={async (e) => {
                                    e.preventDefault();
                                    this.props.changeCurrentDashboard(
                                      this.state.loadedDashboardId
                                    );
                                    close();
                                    //   await this.handleLogin();
                                    //   console.log(this.state.errorFlag);
                                    //   if (this.state.errorFlag === 0) {

                                    //   }
                                  }}
                                  style={{
                                    marginLeft: "auto",
                                    marginRight: "auto",
                                    // marginBottom: "0px",
                                    // marginTop: "0px",
                                  }}
                                >
                                  Load
                                </Button>
                              </div>
                            </Form>
                          </Card.Body>
                        </Card>
                      )}
                    </Popup>
                  </NavDropdown.Item>
                  <NavDropdown.Item
                    className={
                      Object.entries(this.props.currentDashboard).length == 0 &&
                      "disabled"
                    }
                  >
                    <Popup
                      contentStyle={{ width: "28rem" }}
                      trigger={<p>Edit Dashboard</p>}
                      position="right center"
                      onClose={() =>
                        this.setState({
                          currDashboardName: this.props.currentDashboard.name,
                          currCheckDefault:
                            this.props.currentDashboard.isDefault,
                        })
                      }
                      modal
                    >
                      {(close) => (
                        <Card style={{ maxWidth: "28rem" }}>
                          <Card.Header as="h5" style={{ textAlign: "center" }}>
                            Edit current Dashboard
                          </Card.Header>
                          <Card.Body>
                            <Form>
                              <Form.Group>
                                <Form.Label>Name:</Form.Label>
                                <Form.Control
                                  type="text"
                                  placeholder="Enter name"
                                  value={this.state.currDashboardName}
                                  onChange={this.currDashboardNameHandler}
                                />
                              </Form.Group>

                              <Form.Group controlId="formBasicCheckbox">
                                <Form.Check
                                  type="checkbox"
                                  label="Default"
                                  checked={this.state.currCheckDefault}
                                  onChange={this.currCheckDefaultHandler}
                                />
                              </Form.Group>

                              <div style={{ display: "flex" }}>
                                <Button
                                  variant="primary"
                                  type="submit"
                                  //   onClick={this.handleNewDashbaord}
                                  onClick={async (e) => {
                                    e.preventDefault();
                                    await this.props.editDashboard({
                                      isDefault: this.state.currCheckDefault,
                                      name: this.state.currDashboardName,
                                    });
                                    await this.getlistofDashboards();

                                    // await this.props.addNewDashboard({
                                    //   isDefault: this.state.checkDefault,
                                    //   name: this.state.newDashboardName,
                                    // });
                                    close();
                                    //   await this.handleLogin();
                                    //   console.log(this.state.errorFlag);
                                    //   if (this.state.errorFlag === 0) {

                                    //   }
                                  }}
                                  style={{
                                    marginLeft: "auto",
                                    marginRight: "auto",
                                    // marginBottom: "0px",
                                    // marginTop: "0px",
                                  }}
                                >
                                  Edit
                                </Button>
                              </div>
                            </Form>
                          </Card.Body>
                        </Card>
                      )}
                    </Popup>
                  </NavDropdown.Item>

                  <NavDropdown.Item
                    className={
                      Object.entries(this.props.currentDashboard).length == 0 &&
                      "disabled"
                    }
                  >
                    <Popup
                      contentStyle={{ width: "26rem" }}
                      trigger={<p>Delete Dashboard</p>}
                      position="right center"
                      // onClose={() =>
                      //   this.setState({
                      //     currDashboardName: this.props.currentDashboard.name,
                      //     currCheckDefault: this.props.currentDashboard.isDefault,
                      //   })
                      // }
                      modal
                    >
                      {(close) => (
                        <Card style={{ maxWidth: "26rem" }}>
                          <Card.Header as="h5" style={{ textAlign: "center" }}>
                            Delete current Dashboard
                          </Card.Header>
                          <Card.Body>
                            <Card.Text>
                              Do you want to want to delete current Dashbaord?
                              <br />
                              Note that this action is permanent.
                            </Card.Text>
                            <div className="row">
                              <div className="col">
                                <Button
                                  variant="danger"
                                  type="submit"
                                  //   onClick={this.handleNewDashbaord}
                                  onClick={async (e) => {
                                    e.preventDefault();
                                    const { data: message } = await axios.post(
                                      "http://localhost:8080/api/users/deleteDashboard",
                                      null,
                                      {
                                        params: {
                                          id: this.props.currentDashboard.id,
                                        },
                                      }
                                    );
                                    await window.location.reload();
                                  }}
                                  style={{
                                    marginLeft: "auto",
                                    marginRight: "auto",
                                    // marginBottom: "0px",
                                    // marginTop: "0px",
                                  }}
                                >
                                  Delete
                                </Button>
                              </div>
                              <div className="col">
                                <Button
                                  variant="primary"
                                  type="submit"
                                  //   onClick={this.handleNewDashbaord}
                                  onClick={async (e) => {
                                    e.preventDefault();
                                    // await this.props.editDashboard({
                                    //   isDefault: this.state.currCheckDefault,
                                    //   name: this.state.currDashboardName,
                                    // });
                                    // await this.getlistofDashboards();

                                    // await this.props.addNewDashboard({
                                    //   isDefault: this.state.checkDefault,
                                    //   name: this.state.newDashboardName,
                                    // });
                                    close();
                                    //   await this.handleLogin();
                                    //   console.log(this.state.errorFlag);
                                    //   if (this.state.errorFlag === 0) {

                                    //   }
                                  }}
                                  style={{
                                    marginLeft: "100px",
                                    marginRight: "0px",
                                    // marginBottom: "0px",
                                    // marginTop: "0px",
                                  }}
                                >
                                  Cancel
                                </Button>
                              </div>
                            </div>
                          </Card.Body>
                        </Card>
                      )}
                    </Popup>
                  </NavDropdown.Item>

                  {/* <NavDropdown.Item>Copy</NavDropdown.Item> */}
                </NavDropdown>
              </Nav>
              <Nav>
                <Navbar.Brand>Hi {this.props.activeUser.name}!</Navbar.Brand>

                <Button
                  variant="primary"
                  style={{
                    marginTop: "4px",
                    fontSize: "15px",
                    height: "34px",
                    width: "78px",
                    textAlign: "center",
                  }}
                  onClick={this.props.doLogout}
                >
                  Logout
                </Button>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
      );
    } else {
      navbarshow = (
        <div>
          <Navbar
            collapseOnSelect
            expand="lg"
            bg="dark"
            variant="dark"
            className="fixed-top"
          >
            <Navbar.Brand href="#">OPT Dashboard</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto"></Nav>
              <Nav>
                {/* <Navbar.Brand>Hi Akshit!</Navbar.Brand> */}

                {/* <Button
                    variant="primary"
                    style={{
                        marginTop: "4px",
                      fontSize: "15px",
                      height: "35px",
                      width: "78px",
                      textAlign: "center",
                    }}
                    onClick={this.props.doLogout}
                  >
                    Login
                  </Button> */}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
      );
    }

    return <div  >{navbarshow}</div>;
  }
}

export default NavbarComponent;
