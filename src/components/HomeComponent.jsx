import React, { Component } from "react";
import StackchartMainComponent from "./StackchartMainComponent";
import YTDChartMainComponent from "./YTDChartMainComponent";
import BannerComponent from "./BannerComponent";
import NavbarComponent from "./NavbarComponent";
import DashboardMainComponent from "./DashboardMainComponent";
import LoginForm from "./LoginForm";
import ReactToPrint from "react-to-print";
import "reactjs-popup/dist/index.css";
import axios from "axios";
import { useReactToPrint } from 'react-to-print';
// import Popup from "reactjs-popup";

class HomeComponent extends Component {
  constructor(props){
    super(props)
    this.navref = React.createRef();
    this.dashmainref = React.createRef();
    this.state = {
    isLoggedIn: 0,
    activeUser: new Object(),
    defaultDashboard: new Object(),
    currentDashboard: new Object(),
    hasDashboards: false,
    // testing: 1,
    // userId: -1,
    // Category1: "UBR Level 2"
  };

  }
  

 
  async componentDidMount() {
    const {data: activeUser} = await axios.get("http://localhost:8080/api/users/initialize");
    if(activeUser){
      let isLoggedIn=1;
      this.setState({isLoggedIn,activeUser})
      await this.getDefaultDashboard();
      this.setState({currentDashboard:this.state.defaultDashboard});
    }
  }

  doLogin = async (activeUser) => {
    let isLoggedIn=1;
    this.setState({isLoggedIn,activeUser})
    await this.getDefaultDashboard();
    this.setState({currentDashboard:this.state.defaultDashboard});
    this.navref.current.getlistofDashboards();
  };

  doLogout = async () => {
    // await this.setState({
    //   isLoggedIn: 0,
    //   userId: -1,
    //   userName: "",
    //   Category1: "UBR Level 2"
    // });
    const {data:message} = await axios.post("http://localhost:8080/api/users/logout",{});
    console.log(message);
    let isLoggedIn=0;
    let activeUser = new Object();
    this.setState({isLoggedIn,activeUser})
    await window.location.reload();

  };
  // testf= async () => {
  //   console.log("fuc");
  //   // this.refs.child.f();
  //   this.testref.current.f();
  //   // this.setState();
  // };

  getDefaultDashboard = async() => {
    const {data: defaultDashboard} =  await axios.get("http://localhost:8080/api/users/getDefaultDashboard");
    const {data: hasDashboards} =  await axios.get("http://localhost:8080/api/users/hasDashboards");
    console.log(hasDashboards,"my");
    this.setState({defaultDashboard,hasDashboards});
}

addNewDashboard = async(newDashboard) => {
  console.log(newDashboard,"okk");
  const {data: currentDashboard} =  await axios.post("http://localhost:8080/api/users/addDashboard",newDashboard);
  await this.getDefaultDashboard();
  this.setState({currentDashboard});
}

editDashboard = async(editedDashboard) =>{
  console.log("you trying to edit it");
  const {data: currentDashboard} =  await axios.post("http://localhost:8080/api/users/editDashboard",editedDashboard,{
      params: {
        id: this.state.currentDashboard.id,
      }
  });
  await this.getDefaultDashboard();
  this.setState({currentDashboard});
}
addNewChart = async(newChart) => {
  console.log(newChart,"okk");
  const {data: message} =  await axios.post("http://localhost:8080/api/users/addChart",newChart,{
                params: {id: this.state.currentDashboard.id}
            });
  console.log(message);
  await this.dashmainref.current.getDashboardCharts();
  // await this.getDefaultDashboard();
  // this.setState({currentDashboard});
}

changeCurrentDashboard = async(id) => {
  console.log(id);
  const {data: currentDashboard} =  await axios.get("http://localhost:8080/api/users/getDashboard",{
    params: {
      id: id,
    }
  });
  // await this.getDefaultDashboard();
  await this.setState({currentDashboard});
}

saveCharts = async() =>{
  alert("Dashboard is saved.");
  await this.dashmainref.current.saveCharts1();
}
// handlePrint = useReactToPrint({
//   content: () => this.dashmainref.current,
// });
// handlePrint = async() =>{
//   console.log("printt")
// }

  render() {
    const styleStackchartmain = {
      marginTop: "15px",
      border: "3px solid #030921",
      padding: "40px",
      textAlign: "center",
    };
    const YTDchartmain = {
      marginTop: "15px",
      border: "3px solid #030921",
      padding: "40px",
      textAlign: "center",
    };

    let homeshow;
    if(!this.state.isLoggedIn){
      homeshow = (
        <div>
          <LoginForm
          doLogin={this.doLogin}
          />
        </div>
      );
    }
    else{

      homeshow = (
        <DashboardMainComponent
        ref ={this.dashmainref}
        defaultDashboard= {this.state.defaultDashboard}
        currentDashboard= {this.state.currentDashboard}
        hasDashboards= {this.state.hasDashboards}
        />
      );

    }




    return (
      <div>
        <NavbarComponent
        ref = {this.navref}
          isLoggedIn={this.state.isLoggedIn}
          activeUser={this.state.activeUser}
          currentDashboard={this.state.currentDashboard}
          doLogout={this.doLogout}
          addNewDashboard= {this.addNewDashboard}
          addNewChart= {this.addNewChart}
          changeCurrentDashboard = {this.changeCurrentDashboard}
          saveCharts = {this.saveCharts}
          editDashboard = {this.editDashboard}
          // dashmainref = {this.dashmainref}
          // testf={this.testf}
          // doLogin={this.doLogin}
        />
        <br />
        <br />
        {homeshow}
      </div>
    );
  }
}

export default HomeComponent;
