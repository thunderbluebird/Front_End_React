import React, { Component } from "react";
import axios from "axios";
import DashboardComponent from "./DashboardComponent";
import BannerComponent from "./BannerComponent";
import { Card } from "react-bootstrap";
class DashboardMainComponent extends Component {
  constructor(props) {
    super(props);
    this.dashref = React.createRef();
    this.state = {
      // defaultDashboard: new Object(),
      // currentDashboard: new Object()
      AllCharts: [],
    };
  }

  async componentDidMount() {
    await this.getDashboardCharts();
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.currentDashboard != this.props.currentDashboard) {
      await this.getDashboardCharts();
      console.log(this.state.AllCharts);
    }
  }

  //   f = async () =>{
  //       console.log("hmm");
  //       const {data: test} = await axios.get("http://localhost:8080/api/users/test1");

  //       this.setState({test})
  //   }

  // I'll create a function that gets the default dashboard

  getDashboardCharts = async () => {
    if (Object.entries(this.props.currentDashboard).length) {
      const { data: AllCharts } = await axios.get(
        "http://localhost:8080/api/users/getListOfCharts",
        {
          params: { id: this.props.currentDashboard.id },
        }
      );
      console.log(AllCharts);
      for (let ch of AllCharts) {
        const { data: filtervals } = await axios.get(
          "http://localhost:8080/api/users/getChartFilters",
          {
            params: { id: ch.id },
          }
        );
        ch["filtervals"] = filtervals;
      }
      console.log(AllCharts);
      await this.setState({ AllCharts });
    } else {
      await this.setState({ AllCharts: [] });
    }
  };

  saveCharts1 = async () => {
    await this.dashref.current.saveCharts2();
  };

  render() {
    const stylePage = {
      // marginTop: "25px",
      marginLeft: "20px",
      marginRight: "20px",
    };
    const styleCard = {
      marginTop: "90px",
      marginLeft: "auto",
      marginRight: "auto",
      // margin: "auto",
      // textAlign: "center",
      //   width: 50%;
      height: "200px",
      width: "415px",
      fontSize: "28px",
    };
    let dashboardShow;
    console.log(this.props);
    console.log(this.props.currentDashboard);
    if (Object.entries(this.props.currentDashboard).length) {
      console.log(this.props.currentDashboard);
      dashboardShow = (
        <div>
          <DashboardComponent
            ref={this.dashref}
            dashboard={this.props.currentDashboard}
            AllCharts={this.state.AllCharts}
          />
        </div>
      );
    } else if (this.props.hasDashboards) {
      dashboardShow = (
        <div>
          <Card body style={styleCard}>
            {/* {" "} */}
            You don't have any default dashboard. Go to Dashboard- Load
            Dashboard to load one.
          </Card>
        </div>
      );
    } else {
      dashboardShow = (
        <div>
          <Card body style={styleCard}>
            You have not added any dashboards. Go to File- Add Dashboard to add
            one.
          </Card>
        </div>
      );
    }
    return <div style={stylePage}>
      {dashboardShow}
      </div>;
  }
}

export default DashboardMainComponent;
