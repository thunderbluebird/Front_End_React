import React, { Component } from "react";
import { render } from "react-dom";
import { ArrowUpCircle, ArrowDownCircle } from "react-bootstrap-icons";

class DrillButtonsComponent extends Component {
  render() {
    const styledrill = {
      size: "100px",
    };
    return (
      <div className="row">
        <div className="col-lg-5">
          <button
        //   class="btn btn-light"
            style={styledrill}
            onClick={() => this.props.drillFunction(1)}
          >
            <h5>
              <ArrowUpCircle />
            </h5>
          </button>
        </div>
        <div className="col-lg-5">
          <button
            style={styledrill}
            onClick={() => this.props.drillFunction(0)}
          >
            <h5>
              <ArrowDownCircle />
            </h5>
          </button>
        </div>
      </div>
    );
  }
}

export default DrillButtonsComponent;
