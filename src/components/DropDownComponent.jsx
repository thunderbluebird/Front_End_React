import React, { Component } from 'react';
import { Multiselect } from 'multiselect-react-dropdown';


class DropDownComponent extends Component {

    onSelect = (selectedOptions) => {
        // console.log(selectedOptions)
        this.props.updateGraph(selectedOptions,this.props.id);
    }
  
    onRemove = (selectedOptions) => {
        //   console.log(selectedOptions)
        this.props.updateGraph(selectedOptions,this.props.id);
    }

    render(){

        const styleDropDown={
            fontSize: "15px",
        }
        console.log(this.props.options,"idk ");
        return(
            <div style = {styleDropDown} >
                <Multiselect
		        options = {this.props.options} 
		        selectedValues = {this.props.selectedValues} 
		        onSelect = {this.onSelect}
		        onRemove = {this.onRemove}
		        displayValue={this.props.name}
		        showCheckbox ={true}
		        ref={this.multiselectRef}
                style = {{chips: { fontSize: "15px" }}}
		        />

            </div>
            
        );
    }

}

export default DropDownComponent;