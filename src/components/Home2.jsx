import React, { Component } from "react";
import StackchartMainComponent from "./StackchartMainComponent";
import YTDChartMainComponent from "./YTDChartMainComponent";
import BannerComponent from "./BannerComponent";
import ReactToPrint from "react-to-print";

class Home2 extends Component {
  state = {};

  render() {
    const styleStackchartmain = {
      marginTop: "15px",
      border: "3px solid #030921",
      padding: "40px",
      textAlign: "center",
      // fontSize: '16px',
      // fontWeight: 'bold',
      // color: 'black',
    };
    const YTDchartmain = {
      marginTop: "15px",
      border: "3px solid #030921",
      padding: "40px",
      textAlign: "center",
    };
    let filtervals=["Total","Level_2_Banking", "Level_2_Infrastructure"];
    return (
      <div>
        <div ref={(el) => (this.componentRef = el)}>
          <BannerComponent />
          <div style={styleStackchartmain}>
            <StackchartMainComponent
              Category1="UBR Level 2"
              Category2="Total"
              viewname="Domain"
              filtervals={filtervals}
            />
          </div>
          <div className="page-break" > </div>
          {/* <div style={styleStackchartmain}>
            <StackchartMainComponent
              Category1="Country Desc"
              viewname="Location"
            />
          </div> */}
          <div className="page-break" > </div>

          {/* <div style={YTDchartmain}>
            <YTDChartMainComponent
               year="2021"
               Category= "Sub BU"
               PositionIDStatus="Open - Approved"
               colorbar="#085da6"
            />
          </div>
          <div className="page-break" > </div> */}

          {/* <div style={YTDchartmain}>
            <YTDChartMainComponent
              PositionIDStatus="Open - Approved"
              colorbar="#32686b"
            />
          </div> */}
          <div className="page-break" > </div>

          {/* <div style={YTDchartmain}>
            <YTDChartMainComponent
              PositionIDStatus="On Hold"
              colorbar="#5b577d"
            />
          </div> */}
        </div>
        <div style = {{display: "flex"}}>
        <ReactToPrint
          trigger={() => <button className="btn btn-success btn-lg" style = {{marginLeft: "auto", marginRight: "auto", marginBottom: "55px", marginTop: "65px"}}>Publish</button>}
          content={() => this.componentRef}
        />
        </div>
        
      </div>
    );
  }
}

export default Home2;
