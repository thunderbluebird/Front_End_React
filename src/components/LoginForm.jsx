import React, { Component } from "react";
import {
  Button,
  Navbar,
  Nav,
  Form,
  FormControl,
  Card,
  Alert,
} from "react-bootstrap";
import axios from "axios";

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loginEmail: "",
      loginPassword: "",
      errorFlag: 0,
    };
  }
  changeloginEmailHandler = (event) => {
    this.setState({ loginEmail: event.target.value });
  };

  changeloginPasswordHandler = (event) => {
    this.setState({ loginPassword: event.target.value });
  };
  handleLogin = async (e) => {
    e.preventDefault();
    console.log("logged");
    console.log(this.state.loginEmail, this.state.loginPassword);
    const { data: User } = await axios.post(
      "http://localhost:8080/api/users/login",
      {
        email: this.state.loginEmail,
        password: this.state.loginPassword,
      }
    );
    if (!User) {
      let errorFlag = 1;
      this.setState({ errorFlag });
    } else {
      let errorFlag = 0;
      this.setState({ errorFlag });
      console.log(this.props);
      this.props.doLogin(User);
    }
  };

  render() {
    const styleForm = {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      // height: "2000px"
      // border: 3px solid green;
    };
    const styletext = {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      fontSize: "25px",
      fontWeight: "400",
      fontFamily:
        "'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif",
    };

    return (
      <div>
        <br />
        <p style={styletext}>
          Welcome to OPT Dashboard! Please login to continue
        </p>
        <div style={styleForm}>
          <Card style={{ width: "400px" }}>
            <Card.Header as="h5" style={{ textAlign: "center" }}>
              Login
            </Card.Header>
            <Card.Body>
              <Form>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email address:</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={this.state.loginEmail}
                    onChange={this.changeloginEmailHandler}
                  />
                  {/* <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                  </Form.Text> */}
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password:</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={this.state.loginPassword}
                    onChange={this.changeloginPasswordHandler}
                  />
                </Form.Group>

                {this.state.errorFlag > 0 && (
                  <Alert
                    variant="danger"
                    style={{ height: "35px", marginTop: "25px" }}
                  >
                    <p
                      style={{
                        marginTop: "-8px",
                        textAlign: "center",
                        fontSize: "16px",
                      }}
                    >
                      Please enter valid credentials
                    </p>
                  </Alert>
                )}

                <div style={{ display: "flex" }}>
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={this.handleLogin}
                    // onClick={async (e) => {
                    //   e.preventDefault();
                    //   await this.handleLogin();
                    //   console.log(this.state.errorFlag);
                    //   if (this.state.errorFlag === 0) {
                    //     close();
                    //   }
                    // }}
                    style={{
                      marginLeft: "auto",
                      marginRight: "auto",
                      marginBottom: "10px",
                      marginTop: "10px",
                    }}
                  >
                    Submit
                  </Button>
                </div>
              </Form>
            </Card.Body>
          </Card>
        </div>
      </div>
    );
  }
}

export default LoginForm;
