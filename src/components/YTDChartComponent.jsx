import React, { Component } from "react";
import {
  XAxis,
  YAxis,
  BarChart,
  CartesianGrid,
  Legend,
  Bar,
  LabelList,
  Tooltip,
} from "recharts";
const Months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
  "Grand Total",
];

class YTDChartComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const styleYTDChart1 = {
      border: "1px solid #030921",
      padding: "0px",
      textAlign: "center",
      width: "25%",
    };
    const styleYTDChart2 = {
      border: "1px solid #030921",
      padding: "0px",
      textAlign: "center",
      width: "20%",
    };
    const styleYTDChart3 = {
      border: "1px solid #030921",
      padding: "0px",
      textAlign: "center",
      width: "13.599999995%",
    };
    const styleText = {
      fontSize: "15px",
      marginBottom: "8px"
    };
    const monthlis = [];
    for (let m of Months) {
      monthlis.push(
        <div>
          <div style={styleText}>{m}</div>
        </div>
      );
    }
    let w = 160;
    let styleYTDChart;
    if (this.props.CategoryOptions.length >= 6) {
      // cls = "col-lg-1.5";
      styleYTDChart=styleYTDChart3
      w = 80;
    } else if (this.props.CategoryOptions.length >= 4){
      // cls = "col-lg-2.5";
      styleYTDChart=styleYTDChart2
      w = 110;
    }
    else{
      styleYTDChart=styleYTDChart1
      w = 160;
    }
    const lis1 = [];
    const lis2 = [];
    lis1.push(
      <div className="col-lg-2" style={styleYTDChart}>
        Month
      </div>
    );
    lis2.push(
      <div className="col-lg-2" style={styleYTDChart}>
        {monthlis}
      </div>
    );
    let cls = "";
    console.log(this.props.CategoryOptions.length);
   
    for (let i of this.props.CategoryOptions) {
      lis1.push(
        <div className={cls} style={styleYTDChart}>
          {i}
        </div>
      );
      lis2.push(
        <div className={cls} style={styleYTDChart}>
          <BarChart
            width={w}
            height={390}
            data={this.props.data}
            layout="vertical"
          >
            <XAxis type="number" hide="true" />
            <YAxis type="category" dataKey="month" hide="true" />
            <Tooltip />
            <CartesianGrid strokeDasharray="3 3" />
            <Bar dataKey={i} stackId="x" fill={this.props.colorbar}>
              <LabelList dataKey={i} style={{ fill: "#FFF" }} />
            </Bar>
          </BarChart>
        </div>
      );
    }

    return (
      <div>
        <div className="row" style={{ fontSize: "10px" }}>
          {lis1}
        </div>
        <div className="row">{lis2}</div>
      </div>
    );
  }
}

export default YTDChartComponent;
