import React, { Component } from "react";
import { Multiselect } from "multiselect-react-dropdown";

class SingleDropDownComponent extends Component {
  onSelect = (selectedOptions) => {
    console.log(selectedOptions);
    this.props.updateGraph(selectedOptions);
  };

  onRemove = (selectedOptions) => {
    console.log(selectedOptions);
    this.props.updateGraph(selectedOptions);
  };

  render() {
    // const styleDropDown={
    //     fontSize: "15px",
    // }
    console.log(this.props, "okaka");
    return (
      <div>
        <Multiselect
          options={this.props.options}
          selectedValues={this.props.selectedValues}
          singleSelect
          onSelect={this.onSelect}
          onRemove={this.onRemove}
          displayValue={this.props.name}
          style={{ chips: {fontSize: "20px", fontWeight :"500" }   ,searchBox:{ background: "#ebfaf6" } }}
        />
      </div>
    );
  }
}

export default SingleDropDownComponent;
