import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClipboardList, faFolderOpen, faHandshake, faRestroom } from '@fortawesome/free-solid-svg-icons'
import { faWheelchair } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';

class BannerComponent extends Component {

    constructor(){
        super()
        this.state={
            bannerData: {
                "positionIDStatus": {
                    "Open - Approved": 0,
                    "On Hold": 0,
                    "Offered": 0
                },
                "pwd": {
                    "TRUE": 0,
                    "FALSE": 0
                },
                "genderDesc": {
                    "Female": 0,
                    "Male": 0
                }
            }
        }
    }

    async componentDidMount(){
        const {data: bannerData} = await axios.get("http://localhost:8080/api/v1/bannerdata");
        bannerData.genderDesc.Female = bannerData.genderDesc.Female/ (bannerData.genderDesc.Female+bannerData.genderDesc.Male);
        bannerData.genderDesc.Female*=100;
        bannerData.genderDesc.Female = Math.round(bannerData.genderDesc.Female);
        bannerData.genderDesc.Male = 100-bannerData.genderDesc.Female;
        bannerData.pwd.TRUE = bannerData.pwd.TRUE/ (bannerData.pwd.TRUE+bannerData.pwd.FALSE);
        bannerData.pwd.TRUE*=100;
        bannerData.pwd.TRUE = Math.round(bannerData.pwd.TRUE);
        await this.setState({bannerData});  
    }

    render() {
        const styleBanner = {
            background: '#030921',
          };

        const styleIcon = {
            // margin: "50px",
            marginTop: '10px',
            textAlign: 'center',
            fontSize: '18px',
            // border: '2px solid black',
            color: 'white',
            // background: '#030921',
          };

        const styleFont = {
            marginTop: '7px',
            textAlign: 'center',
            fontSize: '12px',
            fontWeight: 'bold',
            // border: '2px solid black',
            color: 'white',
            // background: '#030921',
        };

        return(
            <div style = {styleBanner}>
                <div className= "row">
                    <div className = "col" style = {styleIcon}><FontAwesomeIcon icon={faHandshake} size="3x" /></div>
                    <div className = "col" style = {styleIcon}><FontAwesomeIcon icon={faClipboardList} size="3x" /></div>
                    <div className = "col" style = {styleIcon}><FontAwesomeIcon icon={faFolderOpen} size="3x" /></div>
                    <div className = "col" style = {styleIcon}><FontAwesomeIcon icon={faRestroom} size="3x" /></div>
                    <div className = "col" style = {styleIcon}><FontAwesomeIcon icon={faWheelchair} size="3x" /></div>
                </div>

                <div className= "row">
                    <div className = "col" style = {styleFont}>Offered- {this.state.bannerData.positionIDStatus['Offered']}</div>
                    <div className = "col" style = {styleFont}>Open-Approved- {this.state.bannerData.positionIDStatus['Open - Approved']}</div>
                    <div className = "col" style = {styleFont}>On Hold- {this.state.bannerData.positionIDStatus['On Hold']}</div>
                    <div className = "col" style = {styleFont}> M- {this.state.bannerData.genderDesc['Male']}% <span> &nbsp; &nbsp; </span> F-{this.state.bannerData.genderDesc['Female']}% </div>
                    <div className = "col" style = {styleFont}>PWD-{this.state.bannerData.pwd['TRUE']}%</div>
                </div>
            </div>
        )
    }
}

export default BannerComponent;