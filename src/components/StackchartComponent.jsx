import React, { Component } from 'react';
// import {  } from 'chart.js';
import { XAxis, YAxis, BarChart, CartesianGrid, Legend, Bar, LabelList,Tooltip } from 'recharts';


class StackChartComponent extends Component {


    // constructor(props){
    //     super()

    //     this.state = {
    //         data : new Object(),
    //         // colorConfig: new Object(),
    //         // modified_data: []
    //     }
    // }


    render() {

        const styleStackchart = {
            textAlign: 'center',
            fontSize: '13.5px',
            fontWeight: 'bold',
            color: 'black',
        }


        const pos = Object.keys(this.props.colorConfig);
        const lis = [];
        for (let i of pos) {
            lis.push(
                    <Bar dataKey = {i} stackId = "x" fill = {this.props.colorConfig[i]}>
                    <LabelList dataKey ={i} style = {{ fill: "#FFF"}}/>
                    </Bar>
                );
        }
        
        return (
            <div style = {styleStackchart}>
                <BarChart width = {500} height = {400} data = {this.props.data}>
                    <CartesianGrid strokeDasharray = "3 3"/>
                    <XAxis dataKey = {this.props.Category1} interval = {0} />
                    <YAxis/>
                    <Tooltip/>
                    <Legend />
                    {lis}
                </BarChart>
            </div>
        );
    }
}

export default StackChartComponent;