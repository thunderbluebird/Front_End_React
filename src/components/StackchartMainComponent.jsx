import React, { Component } from "react";
import axios from "axios";
import StackchartComponent from "./StackchartComponent";
import DropDownComponent from "./DropDownComponent";
import DrillButtonsComponent from "./DrillButtonsComponent";
import {
  Button,
  Navbar,
  Nav,
  Form,
  FormControl,
  Card,
  Alert,
} from "react-bootstrap";
import "reactjs-popup/dist/index.css";
import Popup from "reactjs-popup";
const colors = ["lightblue", "orange", "#82b0ff", "green"];

class StackchartMainComponent extends Component {
  constructor(props) {
    super(props);
    console.log(this.props, "emi");
    this.state = {
      // Category1: "UBR Level 2",
      Category1: this.props.Category1,
      Category2: this.props.Category2,
      colorConfig: new Object(),
      data: [],
      Category1Options: [],
      Category1Selected: [],
      Category2Options: [],
      Category2Selected: [],
      // userId: this.props.userId,
    };
  }

  // okay I will first take all data and then apply filter
  async componentDidMount() {
    await this.init();
    // console.log(this.state.userId, this.state.Category1,"omg");
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.stackid != this.props.stackid) {
      console.log("hmmmm");
      await this.setState({
        Category1: this.props.Category1,
        Category2: this.props.Category2,
        colorConfig: new Object(),
        data: [],
        Category1Options: [],
        Category1Selected: [],
        Category2Options: [],
        Category2Selected: [],
      });
      await this.init();
      console.log(this.state.data);
    }

    // console.log(this.state.userId, this.state.Category1,"omg");
  }

  init = async () => {
    let data;
    const { data: data1 } = await axios.get(
      "http://localhost:8080/api/v1/chartdata2",
      {
        params: {
          category1: this.state.Category1,
          category2: this.state.Category2,
        },
      }
    );
    data = data1;
    console.log(data, "first");
    await this.setData(data);
    console.log(this.props.filtervals);
    const { data: data2 } = await axios.get(
      "http://localhost:8080/api/v1/filter",
      {
        params: {
          category1: this.state.Category1,
          category2: this.state.Category2,
          values: this.props.filtervals.toString(),
        },
      }
    );
    if (JSON.stringify(data) !== JSON.stringify(data2)) {
      data = data2;
      await this.setState(data);
      await this.updateData(data);
      console.log(this.state.data, "dtaat");
    }
  };
  setData = (data) => {
    let Category1Options = [];
    let Category1Selected = [];
    let Category2Options = [];
    let Category2Selected = [];
    let colorConfig = {};
    let modifiedData = [];
    for (let key in data) {
      let temp = new Object();
      temp[this.state.Category1] = key;
      Category1Options = [temp, ...Category1Options];
      Category1Selected = [temp, ...Category1Selected];
      let i = 0;
      for (let innerkey in data[key]) {
        temp[innerkey] = data[key][innerkey];
        colorConfig[innerkey] = colors[i];
        i++;
      }
      modifiedData.push(temp);
    }
    // console.log(Category1Options[0]);
    for (let innerkey in data[Category1Options[0][this.state.Category1]]) {
      let temp = new Object();
      temp[this.state.Category2] = innerkey;
      Category2Options = [temp, ...Category2Options];
      Category2Selected = [temp, ...Category2Selected];
    }

    this.setState({
      colorConfig: colorConfig,
      data: modifiedData,
      Category1Options: Category1Options,
      Category1Selected: Category1Selected,
      Category2Options: Category2Options,
      Category2Selected: Category2Selected,
    });
    console.log(this.state.colorConfig, "hiih");
    console.log(this.state.data, "hiih");
    console.log(this.state.Category1Options, "hiih");
    console.log(this.state.Category1Selected, "hiih");
    console.log(this.state.Category2Options, "hiih");
    console.log(this.state.Category2Selected, "hiih");
  };

  updateData = (data) => {
    let Category1Selected = [];
    let Category2Selected = [];
    let colorConfig = {};
    let modifiedData = [];
    if (Object.keys(data).length == 0) {
      this.setState({
        colorConfig: colorConfig,
        data: modifiedData,
      });
      return;
    }
    for (let key in data) {
      let temp = new Object();
      temp[this.state.Category1] = key;
      Category1Selected = [temp, ...Category1Selected];
      let i = 0;
      for (let innerkey in data[key]) {
        temp[innerkey] = data[key][innerkey];
        colorConfig[innerkey] = colors[i];
        i++;
      }
      modifiedData.push(temp);
    }
    for (let innerkey in data[Category1Selected[0][this.state.Category1]]) {
      let temp = new Object();
      temp[this.state.Category2] = innerkey;
      Category2Selected = [temp, ...Category2Selected];
    }
    console.log(Category1Selected, "qwe");
    console.log(Category2Selected, "qwe");

    this.setState({
      colorConfig: colorConfig,
      data: modifiedData,
      Category1Selected: Category1Selected,
      Category2Selected: Category2Selected,
    });
  };

  drillFunction = async (pivot) => {
    // console.log("drill");
    const { data: Category1 } = await axios.get(
      "http://localhost:8080/api/v1/drill",
      {
        params: { label: this.state.Category1, pivot: pivot },
      }
    );
    // console.log(Category1);
    if (Category1 == this.state.Category1) return;
    await this.setState({ Category1: Category1 });
    const { data } = await axios.get(
      "http://localhost:8080/api/v1/chartdata2",
      {
        params: {
          category1: this.state.Category1,
          category2: this.state.Category2,
        },
      }
    );
    // console.log(data);
    await this.setData(data);
  };

  updateGraph = async (selectedItems, id) => {
    if (id === "1") {
      await this.setState({ Category1Selected: selectedItems });
    } else {
      await this.setState({ Category2Selected: selectedItems });
    }

    console.log(this.state.Category1Selected, "qweee");
    console.log(this.state.Category2Selected, "qweee");
    const allSelected = [
      ...this.state.Category1Selected,
      ...this.state.Category2Selected,
    ];
    // console.log(allSelected,"hi");
    let queryParams = [];
    for (let it of allSelected) {
      queryParams = [...queryParams, Object.values(it)[0]];
    }
    console.log(queryParams, "uery");
    const { data } = await axios.get("http://localhost:8080/api/v1/filter", {
      params: {
        category1: this.state.Category1,
        category2: this.state.Category2,
        values: queryParams.toString(),
      },
    });
    console.log(data, "filter");
    await this.setState(data);
    await this.updateData(data);
  };

  saveGraphToDashboard = async () => {
    // await this.stackref.current.saveCharts3();
    // console.log("really");
    console.log(this.state.Category1, "okkkkkkk");
    const { data: message1 } = await axios.post(
      "http://localhost:8080/api/users/updateStackChartCategory1",
      null,
      {
        params: {
          id: this.props.stackid,
          Category1: this.state.Category1,
        },
      }
    );
    let newFilterList = [];
    for (let i of this.state.Category1Selected) {
      let temp = new Object();
      temp["category"] = this.state.Category1;
      temp["name"] = i[this.state.Category1];
      newFilterList = [...newFilterList, temp];
    }
    for (let i of this.state.Category2Selected) {
      let temp = new Object();
      temp["category"] = this.state.Category2;
      temp["name"] = i[this.state.Category2];
      newFilterList = [...newFilterList, temp];
    }
    console.log(newFilterList);
    const { data: message2 } = await axios.post(
      "http://localhost:8080/api/users/updateStackChartFilters",
      newFilterList,
      {
        params: {
          id: this.props.stackid,
        },
      }
    );
  };

  render() {
    const styletext1 = {
      fontSize: "16px",
      fontWeight: "bold",
      // textDecoration: "underline",

      fontFamily:
        "'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif",
    };
    const styletext2 = {
      fontSize: "14px",
      fontWeight: "bold",

      fontFamily:
        "'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif",
    };

    const styletext3 = {
      fontSize: "14px",
      fontWeight: "bold",
      // fontStyle: "italic",
        textAlign: "center",

      fontFamily:
        "'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif",
    };
    const styleChartHead = {
      // marginTop: "15px",
      borderBottom: "2px solid #030921",
      marginBottom: "33px",
      // padding: "10px",
      // textAlign: "center",
      // fontSize: '16px',
      // fontWeight: 'bold',
      // color: 'black',
    };
    return (
      <div>
        {this.props.posi==="left" && (
          <div className="row" style={styleChartHead}>
          <div className="col-lg-7">
            <p style={styletext1}> {this.props.viewname} </p>
            <p style={styletext2}>
              {this.state.Category1} by {this.state.Category2}
            </p>
          </div>
          <div className="col-lg-5 hide">
            <div className="row">
              {/* <div className="col-lg-7">
                <SingleDropDownComponent
                  options={this.state.YearOptions}
                  selectedValues={this.state.YearSelected}
                  name="Year"
                  updateGraph={this.updateGraph}
                />
                <p style={styletext3}> Select Year </p>
              </div> */}
              <div className="col-lg-2"></div>
              <div className="col-lg-5" style={{ marginTop: "1px" }}>
                <DrillButtonsComponent drillFunction={this.drillFunction} />
              </div>
              {/* <div className="col-lg-1"></div> */}
              <div className="col-lg-5">
                <Popup
                  contentStyle={{ width: "35rem" }}
                  trigger={(open) => (
                    <Button variant="primary" style={{ fontSize: "19px" }}>
                      Filter{" "}
                    </Button>

                    // <button className="button">
                    //   Trigger
                    // </button>
                  )}
                  position="right center"
                  closeOnDocumentClick
                >
                  <div>
                    <p style={styletext3}>{this.state.Category1} Filter</p>
                    <DropDownComponent
                      id="1"
                      options={this.state.Category1Options}
                      selectedValues={this.state.Category1Selected}
                      name={this.state.Category1}
                      updateGraph={this.updateGraph}
                    />
                    <br />
                    {this.props.isStack && (
                      <div>
                        <p style={styletext3}>{this.state.Category2} Filter</p>
                        <DropDownComponent
                          className="invisible"
                          id="2"
                          options={this.state.Category2Options}
                          selectedValues={this.state.Category2Selected}
                          name={this.state.Category2}
                          updateGraph={this.updateGraph}
                        />
                      </div>
                    )}

                    {/* <SingleDropDownComponent
                  options={this.state.YearOptions}
                  selectedValues={this.state.YearSelected}
                  name="Year"
                  updateGraph={this.updateGraph}
                />
                <p style={styletext3}> Select Year </p> */}
                  </div>
                  {/* <span> Popup content </span> */}
                </Popup>
              </div>
            </div>
          </div>
        </div>
        )}
        

        {this.props.posi==="right" && (
          <div className="row" style={styleChartHead}>
         
          <div className="col-lg-5 hide">
            <div className="row">
             
              {/* <div className="col-lg-1"></div> */}
              <div className="col-lg-4">
                <Popup
                  contentStyle={{ width: "35rem" }}
                  trigger={(open) => (
                    <Button variant="primary" style={{ fontSize: "19px" }}>
                      Filter{" "}
                    </Button>

                    // <button className="button">
                    //   Trigger
                    // </button>
                  )}
                  position="left center"
                  closeOnDocumentClick
                >
                  <div>
                    <p style={styletext3}>{this.state.Category1} Filter</p>
                    <DropDownComponent
                      id="1"
                      options={this.state.Category1Options}
                      selectedValues={this.state.Category1Selected}
                      name={this.state.Category1}
                      updateGraph={this.updateGraph}
                    />
                    <br />
                    {this.props.isStack && (
                      <div>
                        <p style={styletext3}>{this.state.Category2} Filter</p>
                        <DropDownComponent
                          className="invisible"
                          id="2"
                          options={this.state.Category2Options}
                          selectedValues={this.state.Category2Selected}
                          name={this.state.Category2}
                          updateGraph={this.updateGraph}
                        />
                      </div>
                    )}

                    {/* <SingleDropDownComponent
                  options={this.state.YearOptions}
                  selectedValues={this.state.YearSelected}
                  name="Year"
                  updateGraph={this.updateGraph}
                />
                <p style={styletext3}> Select Year </p> */}
                  </div>
                  {/* <span> Popup content </span> */}
                </Popup>
              </div>
              <div className="col-lg-5" style={{ marginTop: "1px",marginLeft:"9px" }}>
                <DrillButtonsComponent drillFunction={this.drillFunction} />
              </div>
              {/* <div className="col-lg-2"></div> */}

            </div>
          </div>

          <div className="col-lg-7" style = {{      textAlign: "right"}}>
            <p style={styletext1}> {this.props.viewname} </p>
            <p style={styletext2}>
              {this.state.Category1} by {this.state.Category2}
            </p>
          </div>
        </div>
        )}
        

        <StackchartComponent
          colorConfig={this.state.colorConfig}
          data={this.state.data}
          Category1={this.state.Category1}
        />

        {/* 
          <div className="col-lg-4 hide">
            <p style={styletext2}>{this.state.Category1} Filter</p>
            <DropDownComponent
              id="1"
              options={this.state.Category1Options}
              selectedValues={this.state.Category1Selected}
              name={this.state.Category1}
              updateGraph={this.updateGraph}
            />
            <br />
            {this.props.isStack && (
              <div>
                <p style={styletext2}>{this.state.Category2}</p>
                <DropDownComponent
                  className="invisible"
                  id="2"
                  options={this.state.Category2Options}
                  selectedValues={this.state.Category2Selected}
                  name={this.state.Category2}
                  updateGraph={this.updateGraph}
                />
              </div>
            )}
          </div> */}

        {/* <div className="col-lg-1 hide">
            <DrillButtonsComponent drillFunction={this.drillFunction} />
          </div> */}
        {/* </div> */}
      </div>
    );
  }
}

export default StackchartMainComponent;
