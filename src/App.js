import './App.css';
import { Route, Link, Router} from "react-router-dom";
import StackchartMainComponent from './components/StackchartMainComponent';
import HomeComponent from './components/HomeComponent';
import BannerComponent from './components/BannerComponent';
import YTDChartMainComponent from './components/YTDChartMainComponent';
import NavbarComponent from './components/NavbarComponent'
import Home2 from './components/Home2'
// import DashboardComponent from './components/DashboardComponent';
// import LoginComponent from './components/LoginComponent';
// import FrontPageComponent from './components/FrontPageComponent';

function App() {
  return (    
      <div className = "">
      <Route exact path ="/" component = {StackchartMainComponent}/>
      <Route exact path ="/ytd" component = {YTDChartMainComponent}/>
      <Route exact path ="/home" component = {HomeComponent}/>
      <Route exact path ="/nav" component = {NavbarComponent}/>
      <Route exact path ="/home2" component = {Home2}/>
      {/* <Route exact path ="/frontpage" component = {FrontPageComponent}/>
      <Route exact path ="/dashboard" component = {DashboardComponent}/>
      <Route exact path ="/login" component = {LoginComponent}/> */}

    </div>
  );
}

export default App;